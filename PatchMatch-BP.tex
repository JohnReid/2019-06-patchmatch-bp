\documentclass[xcolor={usenames, dvipsnames, svgnames, table}]{beamer}

%
% Customise beamer
\usetheme{metropolis}  % Simple minimal theme
\usecolortheme[snowy]{owl}  % Dark on light
\setbeamertemplate{footline}{}  % Completely remove footline
\setbeamertemplate{frametitle continuation}{[\insertcontinuationcount]}  % Nicer numbering
%
% Uncomment line below to make printable notes
% \setbeameroption{show only notes}
%
% Remove icons from bibliography
% https://tex.stackexchange.com/a/53131/39498
\setbeamertemplate{bibliography item}{}


%
% PDF metadata
\usepackage{hyperref}
\hypersetup{%
  pdfauthor={John Reid},
  % pdftitle={<TITLE>},
  % pdfkeywords={<KEYWORDS>},
  pdfcreator={xelatex},
  % pdfproducer={xelatex},
}


%
% Math
\usepackage{amsmath}  % For general maths
\usepackage{pifont}  % http://ctan.org/pkg/pifont
\usepackage{bm}  % For bold math
\usepackage{esdiff}  % For derivatives
\usepackage{mathtools}
\usepackage{commath}
\usepackage{siunitx}
\usepackage{newtxmath}
%\usepackage{amsthm}
%\usepackage{thmtools}
%\usepackage{wasysym}
%\usepackage{amssymb} % not with newtxmath
%\usepackage{mathabx} % not with newtxmath
\DeclareMathOperator*{\argmax}{arg\,max}
\DeclareMathOperator*{\argmin}{arg\,min}


%
% Miscellaneous
%\usepackage{hanging}  % Hanging paragraphs
\usepackage{booktabs}  % Nice tables
% \usepackage{layouts}   % Info about layouts
\usepackage{graphicx}  % Graphics
\usepackage[normalem]{ulem} % Underlining
% \usepackage{minted}  % For syntax highlighting source code
\usepackage{setspace}  % For setstretch
\usepackage{calc}  % For widthof()
\usepackage{fontawesome}  % For Twitter symbol, etc...
\usepackage{silence}  % To silence some warning
% \WarningFilter{biblatex}{Patching footnotes failed}
\usepackage{filecontents}


%
% TikZ
\usepackage{tikz}
\pgfdeclarelayer{background}
\pgfdeclarelayer{foreground}
\pgfsetlayers{background, main, foreground}
\usetikzlibrary{
  arrows,
  arrows.meta,
  shapes,
  decorations.pathmorphing,
  backgrounds,
  positioning,
  fit,
  shadows,
  calc,
  graphs,
  graphs.standard,
  bayesnet}


%
% Set up biblatex
\usepackage[
  doi=false,
  isbn=false,
  url=false,
  sorting=none,
  style=authoryear,
  backend=biber]{biblatex}
%
% We specify the database.
\addbibresource{2019-06-PatchMatch-BP.bib}  % chktex-file 8
\DeclareCiteCommand{\citejournal}
  {\usebibmacro{prenote}}
  {\usebibmacro{citeindex}%
   \usebibmacro{cite}
   \usebibmacro{journal}}
  {\multicitedelim}
  {\usebibmacro{postnote}}
%
% To cite in frame footnote
\newcommand{\myfootcite}[1]{\footnote[frame]{\cite{#1}}}
%
% A footnote without a marker from
% https://tex.stackexchange.com/questions/30720/footnote-without-a-marker/30726#30726
\newcommand\blfootnote[1]{
  \begingroup
    \renewcommand\thefootnote{}\footnote[frame]{#1}
    \addtocounter{footnote}{-1}
  \endgroup
}


%
% Fonts
%
\usepackage{fix-cm}  % just to avoid some spurious messages
\usefonttheme{professionalfonts}  % using non standard fonts for beamer
%\usefonttheme{serif}  % default family is serif
\setmainfont{TeX Gyre Bonum}


%
% Quotations
% See: https://tex.stackexchange.com/questions/365231/enclose-a-custom-quote-environment-in-quotes-from-csquotes
%
\def\signed#1{{\leavevmode\unskip\nobreak\hfil\penalty50\hskip1em
  \hbox{}\nobreak\hfill #1%
  \parfillskip=0pt \finalhyphendemerits=0 \endgraf}}

  \newsavebox\mybox{}
\newenvironment{aquote}[1]
  {\savebox\mybox{#1}\begin{quote}\openautoquote\hspace*{-.7ex}}
  {\unskip\closeautoquote\vspace*{1mm}\signed{\usebox\mybox}\end{quote}}


%
% COMMANDS
%
% Todo
\newcommand{\todo}[1]{\textbf{TODO}: #1}
\newcommand{\hide}[1]{}
%
% Cpp command
\newcommand{\cpp}{C\nolinebreak\hspace{-.05em}\raisebox{.4ex}{\tiny\bf +}\nolinebreak\hspace{-.10em}\raisebox{.4ex}{\tiny\bf +}}
%
% Tick and cross
\newcommand{\cmark}{\ding{51}}
\newcommand{\xmark}{\ding{55}}


%
% Document metadata
\title{PatchMatch Belief Propagation}
\subtitle{a probabilistic PatchMatch}
% \date{February 19, 2018}
\author{John Reid}
\institute{Blue Prism}
%
% Fix from https://tex.stackexchange.com/a/444420/39498
\def\titlepage{%
  \usebeamertemplate{title page}
}


%
% Document
\begin{document}

\maketitle


\begin{frame}{PatchMatch}
  \begin{center}
    \includegraphics[width=.85\textwidth]{figures/PatchMatch}
  \end{center}
\end{frame}


\begin{frame}{What is wrong with PatchMatch?}
  \begin{quote}
    ``has difficulty finding reliable correspondences in very large smooth regions''\myfootcite{hacohenNonrigidDenseCorrespondence2011}
  \end{quote}
  \begin{itemize}
    \item Loss has no spatial smoothness even if algorithm does
    \item Requires early-stopping: how to decide?
    \item Worse in more powerful variants
  \end{itemize}
  \emph{PatchMatch Belief Propagation}\myfootcite{bessePMBPPatchMatchBelief2014}
  combines Particle Belief Propagation with PatchMatch
\end{frame}


\begin{frame}{Markov random field}
  \begin{center}
    \input{tikz/PM-MRF.tex}
  \end{center}
\end{frame}


\begin{frame}{Probabilistic model}
  \begin{center}
    $p(\mathbf{u}_1, \ldots, \mathbf{u}_n) = \exp^{-E(\ldots)}$ where $E$ is the energy: \\
    \vspace{4pt}
    \includegraphics[height=40pt]{figures/Besse-energy} \\
    \vspace{10pt}
    data term: \\
    \includegraphics[height=37pt]{figures/Besse-weighted-patch} \\
    \vspace{10pt}
    pairwise smoothing term: \\
    \includegraphics[height=20pt]{figures/Besse-pairwise}
  \end{center}
\end{frame}


\begin{frame}{Results}
  \includegraphics[width=\textwidth]{figures/Besse-fig-1}
\end{frame}


\begin{frame}{Belief propagation (BP)}
  \begin{columns}[t]
    \begin{column}{.27\textwidth}
      \begin{center}
        \input{tikz/belief-propagation}
      \end{center}
    \end{column}
    \begin{column}{.73\textwidth}
      \begin{itemize}
        \item Maintain marginal belief for each node
        \item Combine messages from Markov blanket
        \begin{itemize}
          \item Max-product for MAP estimation
          \item Sum-product for marginal distribution
        \end{itemize}
        \item Iterate until convergence
        \begin{itemize}
          \item Exact algorithm on trees
          \item Loopy belief propagation often works on more complex graphs (e.g.\ grids)
        \end{itemize}
        % \item Junction tree algorithm also possible
      \end{itemize}
    \end{column}
  \end{columns}
  \begin{align*}
    p(\mathbf{u}_{1,1},\ldots,\mathbf{u}_{H,W}|\mathcal{D}) &\neq \prod_{i,j} p(\mathbf{u}_{i,j}|\mathcal{D})
  \end{align*}
\end{frame}


\begin{frame}{Particle belief propagation (PBP)}
  \begin{center}
    log disbelief: \\
    \includegraphics[height=30pt]{figures/Besse-log-disbelief} \\
    \vspace{10pt}
    BP messages: \\
    \includegraphics[height=20pt]{figures/Besse-log-disbelief-messages} \\
    \vspace{10pt}
    PBP messages: \\
    \includegraphics[height=20pt]{figures/Besse-PBP-messages} \\
    \vspace{10pt}
    estimate: \\
    \includegraphics[height=20pt]{figures/Besse-PBP-estimate}
  \end{center}
  \blfootnote{\cite{ihlerParticleBeliefPropagation2009, kothapaMaxProductParticleBelief2011}}
\end{frame}


\begin{frame}{PMBP message calculation: sharing particles}
  \includegraphics[width=\textwidth]{figures/Besse-msg-calc}
\end{frame}


\begin{frame}{Algorithms}
  \includegraphics[width=\textwidth]{figures/Besse-algorithms}
\end{frame}


\begin{frame}{Denoising: performance versus runtime}
  \includegraphics[height=.85\textheight]{figures/Besse-alg-comp}
\end{frame}


\begin{frame}{Different models; different inference methods}
  \begin{center}
    \begin{tabular}{lll}
      \toprule
                & Pairwise terms & Inference  \\
      \midrule
      PatchMatch & \xmark{}      & MLE        \\
      PBP        & \cmark{}      & MCMC       \\
      PMBP       & \cmark{}      & MAP        \\
      \bottomrule
    \end{tabular}
  \end{center}

  Soft-attention for query $\mathbf{q}_i$:
  \begin{align*}
    \frac{1}{\sum_j \exp(\mathbf{q}_i \cdot \mathbf{k}_j)} \sum_j \exp(\mathbf{q}_i \cdot \mathbf{k}_j) \mathbf{v}_j
  \end{align*}

  Some work on particle diversity (exploring multiple modes)\myfootcite{pachecoPreservingModesMessages2014}
\end{frame}


\begin{frame}{Graphical models}
  \note{\
    Be sure to mention these things:
    \begin{itemize}
      \item note 1
      \item note 2
    \end{itemize}
  }
  \begin{columns}[t]
    \begin{column}{.5\textwidth}
      \begin{center}
        Bayesian network \\
        \vspace{20pt}
        \input{tikz/model_pca}
      \end{center}
    \end{column}
    \begin{column}{.5\textwidth}
      \begin{center}
        Markov random field \\
        \vspace{20pt}
        \input{tikz/markov-random-field}
      \end{center}
    \end{column}
  \end{columns}
  \blfootnote{Nice course notes:~\cite{ErmonGroupCS228}}
\end{frame}


\begin{frame}{Factor graphs}
  \vspace{-20pt}
  \begin{columns}[t]
    \begin{column}{.5\textwidth}
      \begin{center}
        Markov random field \\
        \vspace{20pt}
        \input{tikz/markov-random-field}
      \end{center}
    \end{column}
    \begin{column}{.5\textwidth}
      \begin{center}
        Factor graph \\
        \vspace{20pt}
        \input{tikz/factor-graph}
      \end{center}
    \end{column}
  \end{columns}
  \begin{align*}
    \log p(Z) &= - \sum_i \log \psi_i(\mathcal{F}_i), \qquad\ \mathcal{F}_i \subseteq Z = \{A, B, C, D, E\}
  \end{align*}
\end{frame}


% \section{References}
\begin{frame}[allowframebreaks]{References}
  % \renewcommand*{\bibfont}{\scriptsize}
  \renewcommand*{\bibfont}{\footnotesize}
  \printbibliography[heading=none]{}
\end{frame}

\end{document}
